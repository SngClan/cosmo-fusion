<?php
/**
 * CosmoFusion functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package CosmoFusion
 */

if ( ! function_exists( 'cosmo_fusion_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cosmo_fusion_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on CosmoFusion, use a find and replace
		 * to change 'cosmo-fusion' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cosmo-fusion', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		set_post_thumbnail_size(1043);

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'cosmo-fusion' ),
			'menu-2' => esc_html__( 'Social menu', 'cosmo-fusion' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'cosmo_fusion_custom_background_args', array(
			'default-color' => 'e7edf3',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

        add_theme_support( 'wp-block-styles' );

        add_theme_support('editor-styles');

        add_theme_support('responsive-embeds');

        add_editor_style(array('css/editor-style.css', cosmo_fusion_fonts_url()));

        add_theme_support( 'editor-color-palette', array(
            array(
                'name' => esc_html__( 'Dark grey', 'cosmo-fusion' ),
                'slug' => 'dark-grey',
                'color' => '#242424',
            ),
            array(
                'name' => esc_html__( 'Grey', 'cosmo-fusion' ),
                'slug' => 'grey',
                'color' => '#7a8899',
            ),
            array(
                'name' => esc_html__( 'Orange', 'cosmo-fusion' ),
                'slug' => 'orange',
                'color' => '#ff510d',
            ),
            array(
                'name' => esc_html__( 'Purple', 'cosmo-fusion' ),
                'slug' => 'purple',
                'color' => '#801fca',
            ),
            array(
                'name' => esc_html__( 'White', 'cosmo-fusion' ),
                'slug' => 'white',
                'color' => '#ffffff',
            ),
            array(
                'name' => esc_html__( 'Black', 'cosmo-fusion' ),
                'slug' => 'black',
                'color' => '#000000',
            ),
        ) );
	}
endif;
add_action( 'after_setup_theme', 'cosmo_fusion_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cosmo_fusion_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'cosmo_fusion_content_width', 1043 );
}
add_action( 'after_setup_theme', 'cosmo_fusion_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cosmo_fusion_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'cosmo-fusion' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'cosmo-fusion' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'cosmo_fusion_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function cosmo_fusion_scripts() {
    
    wp_enqueue_style( 'font-awesome', '//use.fontawesome.com/releases/v5.8.1/css/all.css', array(), '5.8.1' );
    
	wp_enqueue_style( 'cosmo-fusion-fonts', cosmo_fusion_fonts_url() );
    
	wp_enqueue_style( 'cosmo-fusion-style', get_stylesheet_uri(), array(), cosmo_fusion_get_theme_version() );

	wp_enqueue_script( 'cosmo-fusion-functions', get_template_directory_uri() . '/js/functions.js', array('jquery'), cosmo_fusion_get_theme_version(), true );

	wp_enqueue_script( 'cosmo-fusion-navigation', get_template_directory_uri() . '/js/navigation.js', array(), cosmo_fusion_get_theme_version(), true );

	wp_enqueue_script( 'cosmo-fusion-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), cosmo_fusion_get_theme_version(), true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'cosmo_fusion_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

function cosmo_fusion_get_theme_version() {
    $theme_info = wp_get_theme( get_template() );
    return $theme_info->get( 'Version' );
}

if ( ! function_exists( 'cosmo_fusion_fonts_url' ) ) :
	/**
	 * Register Google fonts for Cosmo Fusion.
	 *
	 * Create your own cosmo_fusion_fonts_url() function to override in a child theme.
	 *
	 * @since cosmo-fusion 1.0.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function cosmo_fusion_fonts_url() {
		$fonts_url     = '';
		$font_families = array();
		/**
		 * Translators: If there are characters in your language that are not
		 * supported by Ubuntu, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$font1 = esc_html_x( 'on', 'Ubuntu font: on or off', 'cosmo-fusion' );
		if ( 'off' !== $font1 ) {
			$font_families[] = 'Ubuntu:400,400i,500,500i,700,700i';
		}		
		$query_args    = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext,cyrillic' ),
		);
		if ( $font_families ) {
			$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
		}
		return esc_url_raw( $fonts_url );
	}
endif;

/**
 * Filters the title of the default page template displayed in the drop-down.
 */
function cosmo_fusion_default_page_template_title() {
    return esc_html__( 'Default', 'cosmo-fusion' );
}
add_filter( 'default_page_template_title', 'cosmo_fusion_default_page_template_title' );