=== CosmoFusion ===

Contributors: MasterBundles
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready, block-styles
Requires at least: 4.9
Tested up to: 5.2
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Hot offer! Gain a modern and stylish Free WordPress Landing Theme!

Multifunctional and clean, with this WP product, you’ll create an inviting atmosphere on your landing. No one seeing it will refuse to look closer at possible propositions, scrolling your website.

It’s a perfect match for commercial purposes. The theme is already powered with everything from smooth navigation, action buttons to the carousel and video forms. So, you can save your time greatly!

We’ve tested it on a hundred different small businesses and they all say that this is the ideal structure for selling different goods and services. And YES, a blog based on it will be also very beautiful

== Description ==

Description

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

= 1.0 - May 29 2019 =
* Initial release

== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
