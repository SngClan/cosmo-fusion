<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CosmoFusion
 */

?>
        </div>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <div class="container">
            <div class="site-info">
                <p>
                    <a href="<?php echo esc_url( __( 'https://masterbundles.com/', 'cosmo-fusion' ) ); ?>" rel="nofollow">
                        <?php
                        /* translators: %s: author name. */
                        printf( esc_html__( 'by %s', 'cosmo-fusion' ), 'MasterBundles' );
                        ?>
                    </a>
                </p>
            </div><!-- .site-info -->
            <?php
            if(has_nav_menu('menu-2')){
                wp_nav_menu(array(
                    'theme_location' => 'menu-2',
                    'container_class' => 'social-menu-container',
                    'menu_class' => 'theme-social-menu',
                    'depth' => 1,
                    'link_before' => '<span class="menu-text">',
                    'link_after' => '</span>'
                ));
            }
            ?>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
